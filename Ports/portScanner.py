#!/usr/bin/env python3
import sys
sys.path.insert(0, '../src')
import socket


def get_accessible_ports(address, min_port, max_port):
    found_ports = []

    # write code here
    for i in range(min_port, max_port):
        s = socket.socket()
        s.connect((address, i))
        if s.recv(1024) != 0:
            found_ports.append(i)
            i+=1
            print(found_ports)
        
    return found_ports

def main(argv):
    address = sys.argv[1]
    min_port = int(sys.argv[2])
    max_port = int(sys.argv[3])
    ports = get_accessible_ports(address, min_port, max_port)
    for p in ports:
        print(p)

# This makes sure the main function is not called immediatedly
# when TMC imports this module
if __name__ == "__main__":
    if len(sys.argv) != 4:
        print('usage: python %s address min_port max_port' % sys.argv[0])
    else:
        main(sys.argv)
        print('Adress: ',sys.argv[1])
        print('min port: ',sys.argv[2])
        print('max port: ',sys.argv[3])
        get_accessible_ports(sys.argv[1], sys.argv[2], sys.argv[3])
